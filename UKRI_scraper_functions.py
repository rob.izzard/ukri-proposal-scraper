"""

Support functions for UKRI scraper. Please see UKRI_scraper.py
for LICENCE information and disclaimers.

"""
import collections
import configparser
import gzip
import io
import os
import pathlib
import pypdf
import re
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
import selenium
import shutil
import sys
import textwrap
import time
import traceback

from dateutil import parser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver import Firefox
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

############################################################

def date_parse(string):
    """
    Given a string, try to extract a date
    """
    x = None
    try:
        x = parser.parse(string,fuzzy=True)
    except:
        try:
            x = parser.parse(string,fuzzy_with_tokens=True)
        except:
            m = re.search(r'\d{4}-\d{2}-\d{2}', string)
            if m:
                return m.group()
            else:
                return None
    return x

def finite_size(lst):
    # return elements of the list with non-zero size
    return list(filter(lambda x : x.size['height']>0 and x.size['width']>0,
                       lst))




def mkdir(dir):
    """
Make output directory, if it doesn't already exist.
    """
    if not os.path.exists(dir):
        try:
            os.mkdir(dir)
        except:
            sys.exit(f"failed to make data output dir at {dir}")

def gzip_file(path,unlink=True):
    """
    Gzip file at path
    """
    gzpath = pathlib.Path(path).with_suffix('.csv.gz')
    with open(path, 'rb') as f_in:
        with gzip.open(gzpath, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
    if unlink:
        os.unlink(path)

def to_bool(x):
    return x in ("True", "true", True)


# my browser
class Browser():
    def __init__(self,config):
        self.config = config

        # make output directories
        self.outdir = os.path.abspath(os.path.join(config.get('DOWNLOADS','outdir')))
        mkdir(self.outdir)
        if config.getboolean('OPTIONS','screenshots') == True:
            self.screenshot_dir = os.path.abspath(os.path.join(config.get('OPTIONS','screenshot_dir')))
            mkdir(self.screenshot_dir)
        if config.getboolean('OPTIONS','src') == True:
            self.src_dir = os.path.abspath(os.path.join(config.get('OPTIONS','src_dir')))
            mkdir(self.src_dir)

        # make browser options
        opts = selenium.webdriver.firefox.options.Options()
        opts.add_argument('--headless')
        opts.set_preference("browser.download.folderList",
                            2)
        opts.set_preference("browser.download.manager.showWhenStarting",
                            False)

        # https://www.deskriders.dev/posts/1640791840-selenium-firefox-and-saving-webpage-as-pdf/
        opts.set_preference("print.always_print_silent", True);
        opts.set_preference("print.printer_Mozilla_Save_to_PDF.print_to_file", True)
        opts.set_preference("print_printer", "Mozilla Save to PDF")

        opts.set_preference("browser.download.dir",
                            self.outdir)
        opts.set_preference("browser.helperApps.neverAsk.saveToDisk",
                            "text/csv")

        # launch (virtual) browser and make its (virtual) window
        # very large so the sidebar is "shown".
        self.browser = Firefox(options=opts)

        self.browser.set_window_size(1920,1080)

        self.screenshot_counter = 0
        self.src_counter = 0

    def click(self,element):
        # click an element
        self.browser.execute_script("arguments[0].click();", element)


    def click_element(self,
                      xpath,
                      title=None,
                      title_timeout=30,
                      title_render_time=5,
                      render_time=5,
                      pre_status=None,
                      status=None,
                      ref=None):
        """
        Click the first element matching given xpath,
        and then wait either the default render_time
        or, if set, for the title to match after waiting
        for title_render_time.

        If xpath is a list, click each in the list in turn.
        """
        if pre_status != None:
            self.save_status(message=pre_status,
                             ref=ref)

        if isinstance(xpath,list):
            xpaths = xpath
        else:
            xpaths = [xpath]

        for xpath in xpaths:
            self.finite_elements(xpath)[0].click()
            if title != None:
                time.sleep(title_render_time)
                self.waitfortitle(title,
                                  timeout=title_timeout)
            else:
                time.sleep(render_time)

        if status != None:
            self.save_status(message=status,
                             ref=ref)

    def save_status(self,message=None,ref=None):
        """
        Save source and/or screenshot from the "browser".
        """
        if ref != None:
            pre = f"{ref}_"
        else:
            pre = ''

        if self.config.getboolean('OPTIONS','src') == True:
            # save html source
            self.src_counter += 1
            if message:
                file = f"{pre}{self.src_counter}_{message}.html"
            else:
                file = f"{pre}{self.src_counter}.html"
            file = os.path.join(self.src_dir,
                                file)
            with open(file,'w') as f:
                print(self.browser.page_source,
                      file=f)

        if self.config.getboolean('OPTIONS','screenshots') == True:
            # save screenshot
            self.screenshot_counter += 1
            if message:
                file = f"{pre}{self.screenshot_counter}_{message}.png"
            else:
                file = f"{pre}{self.screenshot_counter}.png"
            file = os.path.join(self.screenshot_dir,
                                file)
            self.browser.save_screenshot(file)

        return

    def screenshot(self,message=None):
        """
        Save a screenshot from the "browser".
        """
        if self.config.getboolean('OPTIONS','screenshots') == True:
            self.screenshot_counter += 1
            if message:
                file = f"{self.screenshot_counter}_{message}.png"
            else:
                file = f"{self.screenshot_counter}.png"
            file = os.path.join(self.screenshot_dir,
                                file)
            self.browser.save_screenshot(file)

    def child_elements(self,xpath,root=None):
        # wrapper for find_elements
        if root == None:
            root = self.browser
        return list(root.find_elements(By.XPATH,xpath))

    def finite_elements(self,xpath,root=None):
        # return elements matching xpath with finite size
        if root == None:
            root = self.browser
        return finite_size(root.find_elements(By.XPATH,xpath))

    def all_elements(self,xpath,root=None):
        # return list of all elements matching xpath
        if root == None:
            root = self.browser
        return list(root.find_elements(By.XPATH,xpath))


    def waitforfile(self,
                    filename,
                    timeout=30,
                    dt=0.1,
                    status=None,
                    ref=None):
        """
        Wait for file to exist.
        """
        t = 0
        while t < timeout and not os.path.exists(filename):
            time.sleep(dt)
            t += dt

        if status != None:
            self.save_status(status,ref=ref)

        return os.path.exists(filename)

    def waitfortitle(self,
                     titlestrings,
                     timeout=10,
                     render_time=0,
                     status=None,
                     ref=None):
        """
        Wait for the browser to have the title given in titlestrings list
        """
        n = 0
        time.sleep(render_time) # allow page to render

        # convert titlestrings to list if required
        if not isinstance(titlestrings,list):
            titlestrings = [titlestrings]

        while n < timeout:
            try:
                title = self.browser.title
                for titlestring in titlestrings:
                    if title == titlestring:
                        if status != None:
                            self.save_status(status,
                                             ref=ref)
                        return title
            except:
                pass
            n += 1
            time.sleep(1)

        self.status(message='wait_for_title_failed',
                    ref=ref)
        sys.exit(f"Wait for title timed out (timeout = {timeout}, strings are {titlestrings}).")


    def printpage(self,filename=None):
        """
        Function to print the current page.
        Output is to mozilla.pdf by default, which is moved to
        the filename passed in if that's set.
        """
        if(os.path.exists('mozilla.pdf')):
           print("error, mozilla.pdf already exists!")
           sys.exit()
        self.browser.execute_script("window.print();")
        self.waitforfile('mozilla.pdf')
        if filename:
            os.rename('mozilla.pdf',filename)

    def back(self):
        """
        "Back button".
        """
        self.browser.execute_script("window.history.go(-1)")

    def download_embedded_files(self,
                                xpath='//input[@type="submit"]'):
        """
        Download embedded files.
        """
        files = []
        inputs = self.all_elements(xpath)
        for j,_input in enumerate(inputs):
            file = _input.get_attribute('value')
            print(f"  Downloading embedded file \"{file}\"")
            _input.click()
            time.sleep(5)
            filepath = os.path.join(self.config.get('DOWNLOADS','outdir'),
                                    file)
            files.append(filepath)
        return files

    def print_after_click(self,
                          xpaths,
                          file,
                          predefined_file=None,
                          maxcount=10,
                          string=None,
                          status_string=None,
                          render_sleeptime=5, # time to render the page
                          pdfprint_sleeptime=8 # time to render the PDF
                          ):
        """
    Given an xpath, or a list of xpaths, click it, or them, and print the
    resulting page to a PDF file.

    If given a string, we test the pdf to see if it contains the string.

    We repeat this a maximum of maxcount times before failing.

    If predefined_file is set, the click is assumed to print to this, which
    is then renamed to the required file.
        """
        count = 0
        found = False
        checkfile = None
        while found == False and count < maxcount:
            count += 1
            print(f"Click and print to {file} [attempt {count}]")

            # do clicks
            self.click_element(xpaths,
                               render_time=render_sleeptime)

            if status_string:
                self.save_status(f"{status_string}_open_all_attempt{count}")

            checkfile = file
            if predefined_file != None:
                # file is printed automatically
                self.waitforfile(predefined_file)
                time.sleep(pdfprint_sleeptime) # time to make the PDF
                if file != predefined_file:
                    os.rename(predefined_file,
                              file)
                    checkfile = predefined_file
            else:
                # print to file
                self.printpage(filename=file)
                self.waitforfile(file)
                time.sleep(pdfprint_sleeptime) # time to make the PDF

            # check resulting pdf for string
            if string != None:
                reader = pypdf.PdfReader(checkfile)
                for page in reader.pages:
                    if re.search(string,page.extract_text()):
                        found = True
                        break

            else:
                # no string given, assume found
                found = True

        if not found:
            sys.exit(f"Failed to find required string \"string\" in PDF at \"{file}\" from xpath {xpath} after {count} attempts.")
        else:
            if string:
                print(f"Found at attempt {count} and saved to {file}.")
            else:
                print(f"Saved to {file} (without a string check).")

        return [file]

    def get(self,*args,**kwargs):
        """
        Wrapper for browser.get for the lazy
        """
        return self.browser.get(*args,**kwargs)

    def filename(self,file):
        """
        Wrapper to construct a filename in the outdir.
        """
        return os.path.join(self.config.get('DOWNLOADS','outdir'),
                            file)



    def PDF_header(self,
                   pdffile,
                   ref,
                   my_panel,
                   application):
        """
        Make PDF header page.
        """
        # make header text (newline separated)
        header_text = f"Application {ref} to panel {my_panel}\n"
        for key,value in application.items():
            header_text += f"{key}:\n    {value}\n"
        header_text += f"File:\n{pdffile}\n"

        # make text in a canvas
        width,height = A4 # an assumption!
        packet = io.BytesIO()
        can = canvas.Canvas(packet,
                            pagesize=A4)

        # font and line width setup
        font = 'Helvetica'
        fontsize = 20
        linespacing = 1.5
        fontaspectratio = 0.45 # best guess for your font of choice
        textwidth = int(width / (fontsize * fontaspectratio))

        can.setFont(font,fontsize) # font
        can.setFillColorRGB(0,0,0) # black

        # add the text, but wrapped
        i = 0
        for line in header_text.split('\n'):
            wraps = textwrap.wrap(line,textwidth)
            for wrap in wraps:
                y = height - int(fontsize*linespacing*(2+i))
                can.drawString(0,y,wrap)
                i += 1

        # write the canvas to the page
        can.save()
        packet.seek(0)

        # add header PDF page
        header_page = pypdf.PdfReader(packet)

        return header_page

    def role(self,application):
        """

        """
        role = application.get('Your role',None)
        subdir = None
        download = False # default to no download

        if role == None or role == "":
            # no role, probably the "other" table
            if self.config.getboolean('DOWNLOADS','download_other') == True:
                download = True
                try:
                    subdir = self.config.get('DOWNLOADS','other_subdir')
                except:
                    subdir = ''
        else:
            # we have a role
            if role.casefold() == 'reader':
                if self.config.getboolean('DOWNLOADS','download_reader') == True:
                    download = True
                    try:
                        subdir = self.config.get('DOWNLOADS','reader_subdir')
                    except:
                        subdir = ''
            elif role.casefold() == 'first introducer':
                if self.config.getboolean('DOWNLOADS','download_first_introducer') == True:
                    download = True
                    try:
                        subdir = self.config.get('DOWNLOADS','first_introducer_subdir')
                    except:
                        subdir = ''
            elif role.casefold() == 'conflict':
                download = 'conflict'
            else:
                download = 'unknown'
        return download,subdir

    def login(self):
        """
        Perform login tasks
        """
        # Login screen : get the form elements
        print("Logging you in...")
        self.get(self.config.get('LOGIN','url'))
        form = {
            'username' : self.finite_elements("//input[@id='emailAddress']"),
            'password' : self.finite_elements("//input[@id='password']"),
            'submit'   : self.finite_elements("//button[@id='sign-in-submit-button']"),
        }
        self.save_status('Login')

        # fill in the form
        fillin = {
            'username' : self.config.get('LOGIN','username'),
            'password' : self.config.get('LOGIN','password'),
            }
        for name,element in form.items():
            if not isinstance(element,list):
                element_list = [element]
            else:
                element_list = element

            for element in element_list:
                if name in fillin:
                    element.send_keys(fillin[name])
        self.save_status('Login_filled')

        # login
        form['submit'][0].click()
        self.save_status('Login_submitted')


    def find_panel(self,
                   panel=None):
        """
        Find your panel page, leave the browser on it.
        """

        if panel == None:
            my_panel = self.config.get('LOGIN','panel')
        else:
            my_panel = panel

        # One is either sent to the "funding service" home screen
        # or the "all panels" page at this point. Wait for either.
        matched_title = self.waitfortitle(['Home - The Funding Service',
                                           'All panels'],
                                          render_time=5,
                                          timeout=30,
                                          status='logged_in')
        print("Logged in")

        if matched_title == 'Home - The Funding Service':
            # If we're at the "funding service" homepage,
            # click the panel link and wait for it to load
            self.click_element("//*[@id='panel-tile']/div/a",
                               title="All panels",
                               title_timeout=30)

        self.save_status(message='all_panels')

        # get all the links to various panels, find one
        # matching yours
        print(f"Trying to find your panel {my_panel}")
        panel_links = self.all_elements('//*[@id="main-section-container"]/ul/li/div/div/h2/a')
        matched_panel = False
        for panel in panel_links:
            if(panel.text.startswith(my_panel)):
                # found your panel page, yay!
                print(f"Found panel matching {my_panel}")
                matched_panel = True
                panel.click()

                # this is REALLY slow... so give it 30 seconds
                self.waitfortitle("Panel overview",
                                  timeout=30,
                                  status="panel_overview")

                # found so exit the loop
                break

        if matched_panel == False:
            sys.exit("Failed to match a panel you're in. Check OPTIONS > panel.")

    def get_table_headers(self,index):
        """
        Determine and return the main table headers.

        On the final parse, return None.
        """
        divs = self.finite_elements('//*[@id="applicationsTab"]/div[@class="responsive-table"]')
        print(f"Divs at index {index} are {divs}")
        try:
            div = divs[index]
        except:
            print("Final div parsed")
            return None,None,None

        # extract table headers
        print(f"div index {index} = {div}")
        self.save_status(f"Top_{index}")
        index += 1

        # try to load the div contents, we need to do this each time around
        # because the page context changes (very annoying, but necessary)
        try:
            th = self.finite_elements(f'//table/thead/tr/th',root=div)
        except:
            sys.exit(f"Failed at loop top for table header, index = {index} : likely the page context has changed and you need to reload the divs.")

        # construct the data table headers
        tableheader = [None] * (len(th)+1)
        for i,header in enumerate(th,start=1):
            if header!=None:
                print(f"header {i} : text {header.text}")
                tableheader[i] = header.text

        return div,tableheader,index

    def get_application_data(self,div,tableheader):
        """
        Get and return a dict of application data
        """
        # we now have the panel page, extract links to each application
        # and hence the application data
        tablerows = self.finite_elements('./table/tbody/tr',root=div)

        # fill the application data
        print(f"Found {len(tablerows)} table rows")
        application_data = {}
        for row_n,row in enumerate(tablerows,start=1):
            print(f"Row {row_n}")
            tds = self.finite_elements(f'./td',root=row)

            application_reference = None

            # fill the application_data[ref]
            for col_n,td in enumerate(tds,start=1):
                if col_n < len(tableheader):
                    text = str(td.text)
                    print(f"    Col {col_n} text \"{text}\"")
                    if tableheader[col_n] == 'Application reference':
                        # The "Application reference" column has a link to the application:
                        # store these
                        application_reference = text.strip()
                        application_data[application_reference] = {}
                        a = self.finite_elements(f'./a',root=td)

                        if len(a) > 0:
                            application_data[application_reference]['URL'] = a[0].get_attribute('href')

                    else:
                        # all other columns should be stored
                        application_data[application_reference][tableheader[col_n]] = text

        return application_data

    def make_merged_PDF(self,
                        pdffile,
                        *args,
                        **kwargs):
        """
        Make merged PDF from the filenames passed in.
        """

        # open PDF file
        merged = pypdf.PdfWriter()

        # build merged PDF file
        for pdf in args:
            print(f"  ... \"{pdf}\"")
            merged.append(pdf)
        time.sleep(2) # Mark S. says this is needed (perhaps on Mac)

        # write the merged PDF file and close it
        file = self.filename(pdffile)
        merged.write(file)
        merged.close()

        print(f"  Done merge of pdfs to \"{file}\"")

    def unlink(self,files,log=False,head='Unlike: '):
        """
        Unlink a list of files
        """
        for file in files:
            if os.path.exists(file):
                if log == True:
                    print(f"{head}\"{file}\"")
                os.unlink(file)


    def make_contents(self):
        return

    def strip_blurb(self):
        # we want to strip the various blurb sections... this will
        # naturally be a hack. We can't strip elements (this
        # alters the DOM) but javascript can replace their contents
        # with an empty string.
        #
        # Sometimes this fails, hence we put this in a "try...except"
        # and wait for someone to find out why.
        style_elements = ('border','padding','margin_bottom')

        for element in self.browser.find_elements(By.CLASS_NAME,
                                                  "govuk-details__text"):
            try:
                # get parent element so we can remove formatting
                parent_element = self.browser.execute_script("return arguments[0].parentElement;",
                                                             element)

                self.browser.execute_script("arguments[0].innerText = '';",
                                            element)
                for s in style_elements:
                    for x in (element,parent_element):
                        self.browser.execute_script(f"arguments[0].style.{s} = '0px';",
                                                    x)

            except Exception:
                #traceback.print_exc()
                pass
