# UKRI proposal scraper



## Rationale

The new UKRI "Funding Service" website is designed to improve your life as a reviewer.
Unfortunately, the option to download applications to PDFs containing all the required text  - a very useful feature of the old JeS system -  is no longer available. Instead, you are given only a link (i.e. a URL) in the PDF to another PDF's URL which is, when you're working on a train with no wifi (as is usually the case), not very useful.

This script, `UKRI_scraper.py`, attempts to fix this by logging in for you, downloading the PDFs you require (which you can choose) and putting them in a convenient set of directories. It tries to include PDFs enmbedded in PDFs in a smart way, saving you time and many mouse clicks. Joy!

Now to persuade UKRI to do this for us or provide an API... 

You may also find this script useful to collate all your applications. Its ability to download everything to usefully-named folders in one shot is certainly a plus for someone like me with my limited organisational skills. 

Many thanks to all who have tried to use this and provided debugging information. When you are logged in to the "Funding Service" you may well see something slightly different to me, so your help is very much appreciated.


## Installation



You will need to have installed at least the following Python-3 packages 

* configparser
* pathlib
* PyPDF2
* reportlab
* selenium

You could just run the following from a terminal:

```
pip3 install configparser pathlib PyPDF2 reportlab selenium
```

You will also require Firefox to be installed as we use this as a headless web browser.

Next, download this gitlab repository and enter its directory, e.g.,

```
git clone https://gitlab.com/rob.izzard/ukri-proposal-scraper.git
cd ukri-proposal-scraper
```

## Use

To set things up, you should make a copy of `login_info` and edit your version which requires your username and password to access the UKRI site, as well as your panel ID (e.g. `PAN123`) and perhaps the other data within (please look!). 

Then run the scraper with your login info file as the first argument. For example:

```
cp login_info my_login_info
# edit my_login_info
# ...
./UKRI_scraper.py my_login_info
```

By default, the scraper outputs to the downloads directory `downloads` with subdirectories for the tasks, e.g. `downloads/reader`, `downloads/first_introducer` and `downloads/other`.

On failure, which happens occasionally, you should just rerun the script. You may have to clean up the broken files so they are not skipped. Apologies, it's all a glorified hack! 

Note: you may see a `Stale Element Reference Exception` error. This is something timing out at the UKRI end. Please just rerun the script if this happens: what's already downloaded should not be re-downloaded.


See also the `UKRI_add_contents.py` script if you want to post-process the downloaded files by adding lists of contents to them. 

## Testing

I have tested this on Ubuntu 22.04 running Python 3.10.12. If you are using a different distribution, please try to debug any problems yourself and let me know your fix. I've tried to use native Python libraries everywhere, so please don't expect speed or efficiency.

## Disclaimer

I take no responsibility for anything you do with this software or any its consequences. It is designed to help and is produced on a best effort, written at night, weekends and during holidays, basis ... so please file bug reports here just don't expect quick fixes. 

Feature requests may be considered if I have time.

Note: I fully expect this script to break when the underlying UKRI website changes. As I say above, oh for an API.
 
Copyright Robert Izzard (2024).
