#!/usr/bin/env python3

"""

Script to scrape applications from the UKRI
"Funding Service" website. Unfortunately, UKRI
provide little (i.e. no) support for anyone who'd like
to download the applications they'd like to review
as (complete) PDF files, in one folder, hence this script.
Oh for an API!

Please see the repository's LICENCE for licencing details.

I take ZERO responsibility for any impact of this
software. It is written and distributed on a best
effort "trying to save you time" basis only.

"""

import configparser
import os
import sys
import UKRI_scraper_functions

# read in config filename as first argument
if len(sys.argv) > 1:
    file = sys.argv[1]
    config = configparser.ConfigParser(interpolation=None)
    config.read(file)
else:
    sys.exit("You must specify a configuration file as the first argument.")

# start the headless web browser
UKRI_browser = UKRI_scraper_functions.Browser(config)

# perform login tasks
UKRI_browser.login()

# find your panel page
UKRI_browser.find_panel()

# We're now at your panel page.
# There can be multiple tables in divs. I am assuming here
# that they have the "responsive-table" class... seems so
# at present. Note that these have to be rescanned each time
# because we've likely changed context.
index = 0
keep_looping = True
while keep_looping:
    div,tableheader,index = UKRI_browser.get_table_headers(index)
    if div == None:
        # finished parsing
        break

    # We *assume* the first column is the application
    # reference column.
    application_data = UKRI_browser.get_application_data(div,tableheader)

    # Now we can get the data associated with each application
    _download_count = 0
    _max_downloads = config.get('OPTIONS','max_downloads')
    for ref,application in application_data.items():
        # should we download this application? if so, set download==True
        # and set the subdir if it is in the config
        _download_count += 1
        if _max_downloads != None and \
           _download_count > int(_max_downloads):
            keep_looping = False
            break;

        # determine, from our role, whether to download
        # and to where (the subdir)
        download,subdir = UKRI_browser.role(application)

        # check for conflict
        if download == 'conflict':
            print(f"You conflict with {ref} : skipping")
            continue
        elif download == 'unknown':
            sys.exit(f" Error : Unknown role.")
        elif download != True:
            # skip this for whatever reason
            continue

        # pdffile that's our final destination
        pdffile = os.path.join(subdir,f"{ref}.pdf")

        # skip existing download
        if os.path.exists(UKRI_browser.filename(pdffile)) and \
           config.getboolean('DOWNLOADS','skip_existing') == True:
            print(f"{ref} has already been downloaded, skipping this (because skip_existing == True).")
            continue

        # get URL of this application, make subdir (if required)
        # and make list pdfs to join
        url = application['URL']
        UKRI_scraper_functions.mkdir(UKRI_browser.filename(subdir))
        pdfs = []

        # get the data for this application from the UKRI website
        print(f"Download {ref} from {url}")
        UKRI_browser.get(url)

        # wait for the title to change
        UKRI_browser.waitfortitle(['Application overview',
                                   'Declare conflict: Application overview'],
                                  status=f"Application_{ref}",
                                  ref=ref)

        if UKRI_browser.browser.title.startswith('Declare conflict'):
            # We must declare that we have no conflict of interest
            # or stop.
            if config.getboolean('OPTIONS','always_declare_no_conflict_of_interest') == False:
                # Do not declare conflict of interest : we should warn the user
                print(f"  Application {ref} requires that we declare no conflict of interest before we can do anything with it. You can do this automatically by setting always_declare_no_conflict_of_interest to True, but beware this may not be the correct answer.")
                continue

            # sign the appropriate box
            UKRI_browser.click_element(['//*[@id="conflictSelection-2"]',
                                        '//*[@id="main-section-container"]/form/button'])

        # now we can wait for the page to load
        UKRI_browser.waitfortitle('Application overview',
                                  status=f"Application_overview_{ref}",
                                  ref=ref)

        # check for declared conflict of interest
        conflicts = UKRI_browser.finite_elements('//h2[@id="govuk-notification-conflict-banner"]')
        if len(conflicts) > 0:
            print("You have a conflict with this proposal, so you cannot read it in full.")
            continue

        # perhaps strip the annoying blurb?
        if config.getboolean('OPTIONS','strip_blurb') == True:
            UKRI_browser.strip_blurb()

        # download the PDF of the main document by clicking
        # "print this" : this will be called "mozilla.pdf" so we
        # move this to the appropriate directory with the correct
        #
        # note: The filename starts with _ because it's a temporary
        #       file. we rename to the final PDF when we merge all the
        #       files. This prevents us skipping partially-downloaded
        #       applications.
        print("  Downloading main document PDF")
        pdfs += UKRI_browser.print_after_click(['//*[@id="readApplicationTab-tab-button"]',
                                                '//*[@id="printThis"]'],
                                               UKRI_browser.filename(f"main{ref}.pdf"),
                                               predefined_file='mozilla.pdf')
        UKRI_browser.save_status(f"After print",ref=ref)

        # extract other PDF links in the document, these don't require
        # the printer panel, they're just to be downloaded assuming
        # they are PDFs.
        pdfs += UKRI_browser.download_embedded_files()

        # click the "Reviews" and save them
        if config.getboolean('OPTIONS','save_reviews') == True:
            # click "reviews" then "open all"
            # full 2nd xpath: "/html/body/div/div/div/main/div[3]/div/div[4]/div/div/div/div[1]/button",
            print("Saving reviews")
            pdfs += UKRI_browser.print_after_click(['//*[@id="reviewsTab-tab-button"]',
                                                    "//*[@id='reviews']/div[1]/button"],
                                                   UKRI_browser.filename(f'reviews{ref}.pdf'),
                                                   string='Expert review score',
                                                   status_string=f'Reviews_{ref}')

        if config.getboolean('OPTIONS','save_responses') == True:
            # click the "Responses" and save them
            print("Saving responses")
            pdfs += UKRI_browser.print_after_click('//*[@id="responseTab-tab-button"]',
                                                  UKRI_browser.filename(f'responses{ref}.pdf'),
                                                  string='Your response',
                                                  status_string=f'Responses_{ref}')

        # merge PDFs and save final file
        print(f"Merge pdfs {pdfs}")
        if len(pdfs) > 0:
            print("  Join PDFs")
            UKRI_browser.make_merged_PDF(pdffile,
                                         UKRI_browser.PDF_header(pdffile,
                                                                 ref,
                                                                 config.get('LOGIN','panel'),
                                                                 application),
                                         *pdfs)

            # remove temporary PDF files
            print("  Remove temporary files")
            UKRI_browser.unlink(pdfs + ['mozilla.pdf'],log=True,head='  ... ')

        # go back for the next application
        UKRI_browser.click_element("//*[@class='govuk-back-link ']",
                                   pre_status="pre-Click_back",
                                   status="post-Click_back",
                                   ref=ref)

print(f"\nFinished UKRI_scraper.py!\nFiles should be in {config.get('DOWNLOADS','outdir')}. Please buy me a coffee [or something stronger!] when we meet :)")
