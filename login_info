[LOGIN]

# You need to fill in your username and password
username : <fill this in>
password : <fill this in>

# You need to fill in your panel, e.g. PAN123
panel : <fill this in>

# URL to the main funding service login page
url : https://funding-service.ukri.org/signIn

############################################################

[OPTIONS]

# select the parts to download: you usually want
# all these to be True unless they cause failures
# (e.g. if there are no reviews or responses yet?)
save_reviews: True
save_responses: True

# Automate ticking "no" in conflict of interest box?
# You have probably already checked for this, but
# be warned you should check the application yourself.
always_declare_no_conflict_of_interest : False


### Debugging/bug-reporting options

# save screenshots?
screenshots : False
screenshot_dir : screenshots

# save page HTML source? 
src : False
src_dir : src

## limit number of downloads? only really for debugging!
# max_downloads : None

# strip blurb, i.e. the information about each section?
# You probably don't want to read this *every time* ...
# so default to True
strip_blurb: True

############################################################

[DOWNLOADS]

# main download directory
outdir : downloads

# skip existing downloads?
skip_existing: True

# options to download, and subdirectories
# for these downloads (which are stored
# in the outdir set above).
#
# We start by assuming you want all your
# "first introducer" applications, but then
# you also have "reader" applications that you
# should really read.
#
# Others are called "other". You may want to
# read them if you have a TARDIS from which to
# work.
#
# We default to downloading them all.
#
download_first_introducer : True
first_introducer_subdir : first_introducer
download_reader : True
reader_subdir : reader
download_other : True
other_subdir : other
