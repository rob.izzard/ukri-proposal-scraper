#!/usr/bin/env python3

import configparser
from operator import itemgetter
import os
import pypdf
import re
import sys
import unicodedata
from pypdf.annotations import Link
from pypdf.generic import Fit

############################################################
#
# Script to add contents pages to PDFs
# downloaded using Rob's UKRI scraper.
#
# Config filename is the first argument.
#
# (see UKRI_scraper.py for details of the config.)
#
############################################################
if len(sys.argv) > 1:
    file = sys.argv[1]
    config = configparser.ConfigParser(interpolation=None)
    config.read(file)
else:
    sys.exit("You must specify a configuration file as the first argument.")

# folder containing the documents
dir = os.path.abspath(os.path.join(config.get('DOWNLOADS','outdir')))

subdirs = (
    config.get('DOWNLOADS','first_introducer_subdir'),
    config.get('DOWNLOADS','reader_subdir'),
    config.get('DOWNLOADS','other_subdir')
    )

for subdir in subdirs:
    if subdir != None:
        path = os.path.join(dir,subdir)
        for pdffile in os.listdir(path):
            pdfpath = os.path.join(path,pdffile)
            print(f"pdf: {pdfpath}")

            # open pdf file
            reader = pypdf.PdfReader(pdfpath)

            _found = {
                'review' : {},
                'proposal' : False,
                'sections' : {
                    '1. Details' : False,
                    '2. Core team' : False,
                    '3. Classification of proposal' : False,
                    '4. Vision and Approach' : False,
                    '5. Applicant and team capability to deliver' : False,
                    '6. Ethics' : False,
                    '7. Research involving the use of animals' : False,
                    '8. Conducting research with animals' : False,
                    '9. Resources and Cost' : False,
                    '10. Risk management' : False,
                    '11. Data management and sharing' : False,
                    '12. Other funding support' : False,
                    '13. Project partners' : False,
                    '14. Facilities' : False,
                    '15. References' : False,
                    'Your response' : False,
                }
            }

            for i in range(1,10):
                _found['sections'][f'Review {i}'] = False

            last_numbered_section_at = False
            # find sections
            for page_number,page in enumerate(reader.pages,start=1):
                text = page.extract_text()

                # fix ligatures
                text = text.replace("ﬀ", "ff").\
                    replace("ﬁ", "fi").\
                    replace("ﬂ", "fl").\
                    replace("ﬃ", "ffi").\
                    replace("ﬄ", "ffl").\
                    replace('�', 'fi')

                # find section locations
                for section in _found['sections']:
                    for n in re.findall(section,text):
                        if _found['sections'].get(section) == False:
                            print(f"Found Section \"{section}\" (matches {n}) on page {page_number}")
                            _found['sections'][section] = page_number
                            if re.match('^\d',section):
                                last_numbered_section_at = page_number
                            break

            # find proposal location
            if last_numbered_section_at != False:
                print("Search for proposal")
                for page_number,page in enumerate(reader.pages,start=1):
                    if page_number > last_numbered_section_at:
                        text = page.extract_text()
                        match = re.findall('Application overview',text)
                        if len(match) == 0:
                            print(f" ... found on page {page_number}")
                            if not 'Proposal' in _found['sections']:
                                _found['sections']['Proposal'] = page_number

            # make new pdf file
            outpdfpath = 'with_contents.pdf'
            writer = pypdf.PdfWriter()

            # add all old pdf's data to the new pdf
            for page_number, page in enumerate(reader.pages,start=1):
                writer.add_page(page)

            # add the contents
            for section, page_number in sorted(_found['sections'].items(), key=itemgetter(1)):
                if page_number != False:
                    writer.add_outline_item(title=section,
                                        page_number=page_number-1)
            if _found['proposal'] != False:
                writer.add_outline_item(title='Proposal',
                                        page_number=_found['proposal']-1)

            # write new pdf
            with open(outpdfpath, "wb") as fp:
                writer.write(fp)

            os.rename(outpdfpath,pdfpath)
            print(f"Written {pdfpath} with contents.")
